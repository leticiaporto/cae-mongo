<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servidor_model extends CI_model {
	
	private $database = 'cvtweb';
	private $collection = 'servidores';
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('mongodb');
		$this->conn = $this->mongodb->getConexao();
	}
	
	function buscar() {
		try {
			$filter = [];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
	function pesquisar_id($_id) {
		try {
			$filter = ['_id' => $_id];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);
			
			foreach($result as $servidor) {
				return $servidor;
			}
			
			return NULL;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching servidor: ' . $ex->getMessage(), 500);
		}
	}
	
	function cadastrar($novoId, $siape, $nome, $login, $senha, $cpf, $telefone, $tipo) {
		try {
			$local = array(
				'_id' => $novoId,
				'siape' => $siape,
				'nome' => $nome,
				'login' => $login,
				'senha' => $senha,
				'CPF' => $cpf,	
				'telefone' => $telefone,
				'tipo' => $tipo
			);
			
			$query = new MongoDB\Driver\BulkWrite();
			$query->insert($local);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while saving users: ' . $ex->getMessage(), 500);
		}
	}
	
	function editar($_id, $siape, $nome, $login, $senha, $cpf, $telefone, $tipo) {
		try {
            $dados = array(
                'siape' => $siape,
                'nome' => $nome,
                'login' => $login,
                'CPF' => $cpf,	
                'telefone' => $telefone,
                'tipo' => $tipo
            );

            if($senha){
                $dados['senha'] = $senha;
            }
			$query = new MongoDB\Driver\BulkWrite();
			$query->update(['_id' => $_id], 
			['$set' => 
				$dados
			]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while updating users: ' . $ex->getMessage(), 500);
		}
	}
	
	function excluir($_id) {
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->delete(['_id' => $_id]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	public function lastId(){
		try {
			$filter = [];
			$options = ['sort' => ['_id' => 1]];
			$query = new MongoDB\Driver\Query($filter, $options);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
}