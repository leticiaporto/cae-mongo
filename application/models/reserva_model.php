<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserva_model extends CI_model {
	
	private $database = 'cvtweb';
	private $collection = 'reservas';
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('mongodb');
		$this->conn = $this->mongodb->getConexao();
	}
	
	function buscar() {
		try {
			$filter = [];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
	function pesquisar_id($_id) {
		try {
			$filter = ['_id' =>$_id];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);
			
			foreach($result as $servidor) {
				return $servidor;
			}
			
			return NULL;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching servidor: ' . $ex->getMessage(), 500);
		}
	}
	
	function cadastrar($novoId, $tipo, $status, $responsavel, $dataSolicitacao, $dataInicial, $dataFinal, $dataAutorizacao, $servidorId, $coordenadorId, $localId) {
		try {
            $dados = array(
				'_id' =>  $novoId,
                'localId' => $localId,
				'servidorId' => $servidorId,
                'coordenadorId' => $coordenadorId,
                'dataHoraAutorizacao'=>$dataAutorizacao,
                'tipo' => $tipo,
                'responsavel' => $responsavel,
                'status' => $status,
                'datas' => array(array('datahoraSolicitacao'=>$dataSolicitacao, 'dataHoraInicial'=>$dataInicial, 'dataHoraFinal'=>$dataFinal))
            );
			
			$query = new MongoDB\Driver\BulkWrite();
			
			$query->insert($dados);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while saving users: ' . $ex->getMessage(), 500);
		}
	}
	
	function editar($_id, $tipo, $status, $responsavel, $dataSolicitacao, $dataInicial, $dataFinal, $dataAutorizacao, $servidorId, $coordenadorId, $localId) {
		try {
            $dados = array(
                'localId' => $localId,
				'servidorId' => $servidorId,
                'coordenadorId' => $coordenadorId,
                'dataHoraAutorizacao'=>$dataAutorizacao,
                'tipo' => $tipo,
                'responsavel' => $responsavel,
                'status' => $status,
                'datas' =>  array(array('datahoraSolicitacao'=>$dataSolicitacao, 'dataHoraInicial'=>$dataInicial, 'dataHoraFinal'=>$dataFinal))
            );


			$query = new MongoDB\Driver\BulkWrite();
			$query->update(['_id' =>$_id], 
			['$set' => 
				$dados
			]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while updating users: ' . $ex->getMessage(), 500);
		}
	}
	
	function excluir($_id) {
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->delete(['_id' => $_id]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	public function lastId(){
		try {
			$filter = [];
			$options = ['sort' => ['_id' => 1]];
			$query = new MongoDB\Driver\Query($filter, $options);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
}

