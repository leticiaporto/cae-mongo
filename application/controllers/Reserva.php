<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reserva extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('reserva_model');
        $this->load->model('local_model');
        $this->load->model('servidor_model');
        $this->load->model('coordenador_model');
        $this->load->model('bloco_model');
    }

    function index()
    {
        $reservas = $this->reserva_model->buscar();
        $last_id = $this->local_model->lastId();
        $id = 0;
        foreach($last_id as $r){
            $id  = $r->_id;
        }
        $lista_reservas = array();

        foreach ($reservas as $reserva) {
            
            $lista_reservas['' . $reserva->_id . ''] = $reserva;
            $reserva->servidor = $this->servidor_model->pesquisar_id($reserva->servidorId);
            $reserva->coordenador = $this->coordenador_model->pesquisar_id($reserva->coordenadorId);
            $reserva->local = $this->local_model->pesquisar_id($reserva->localId);           
        }

        $data['reservas'] = $lista_reservas;
        $this->load->view('reserva/reserva_listar', $data);
    }

    public function cadastrar()
    {

        $servidores = $this->servidor_model->buscar();
        $coordenadores = $this->coordenador_model->buscar();
        $locais = $this->local_model->buscar();

        $last_id = $this->reserva_model->lastId();
        $id = 0;
        foreach($last_id as $b){
            $id  = $b->_id;
		}
		$novoId = $id+1;

        $lista_servidores = array('' => 'Selecionar');
        foreach ($servidores as $servidor) {
            $lista_servidores['' . $servidor->_id . ''] = $servidor->nome;
        }

        $data['lista_servidores'] = $lista_servidores;

        $lista_coordenadores = array('' => 'Selecionar');
        foreach ($coordenadores as $coordenador) {
            $lista_coordenadores['' . $coordenador->_id . ''] = $coordenador->servidor->nome;
        }

        $data['lista_coordenadores'] = $lista_coordenadores;

        $lista_locais = array('' => 'Selecionar');
        foreach ($locais as $local) {
            $lista_locais['' . $local->_id . ''] = $local->nome;
        }

        $data['lista_locais'] = $lista_locais;

        if ($this->input->post('btn_cadastrar')) {
            $this->form_validation->set_rules('local', 'Local', 'trim|required');
            $this->form_validation->set_rules('servidor', 'Servidor', 'trim|required');
            $this->form_validation->set_rules('coordenador', 'Coordenador', 'trim|required');
            $this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');
            $this->form_validation->set_rules('responsavel', 'Responsavel', 'trim|required');
            $this->form_validation->set_rules('dataSolicitacao', 'Data Solicitacao', 'trim|required');
            $this->form_validation->set_rules('dataInicial', 'Data Inicial', 'trim|required');
            $this->form_validation->set_rules('dataFinal', 'Data Final', 'trim|required');
            $this->form_validation->set_rules('dataAutorizacao', 'Data Autorizacao', 'trim|required');

            
            $servidor_selecionado = $this->servidor_model->pesquisar_id(intval($this->input->post('servidor')));
            $coordenador_selecionado = $this->coordenador_model->pesquisar_id(intval($this->input->post('coordenador')));
            $local_selecionado = $this->local_model->pesquisar_id(intval($this->input->post('local')));

            $dataSolicitacao = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataSolicitacao')));
            $dataInicial = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataInicial')));
            $dataFinal = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataFinal')));
            $dataAutorizacao = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataAutorizacao')));

          

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->reserva_model->cadastrar(
                    $novoId,
                    $this->input->post('tipo'),
                    $this->input->post('status'),
                    $this->input->post('responsavel'),
                    $dataSolicitacao,
                    $dataInicial,
                    $dataFinal,
                    $dataAutorizacao,
                    $servidor_selecionado->_id,
                    $coordenador_selecionado->_id,
                    $local_selecionado->_id
                );
                if ($result === TRUE) {
                    redirect('reserva');
                } else {
                    $data['error'] = 'Erro ao cadastrar!';
                    $this->load->view('reserva/reserva_cadastrar', $data);
                }
            } else {
                $data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('reserva/reserva_cadastrar', $data);
            }
        } else {
            $this->load->view('reserva/reserva_cadastrar', $data);
        }
    }

    function editar($_id)
    {

        $servidores = $this->servidor_model->buscar();
        $coordenadores = $this->coordenador_model->buscar();
        $locais = $this->local_model->buscar();

        $lista_servidores = array('' => 'Selecionar');
        foreach ($servidores as $servidor) {
            $lista_servidores['' . $servidor->_id . ''] = $servidor->nome;
        }

        $data['lista_servidores'] = $lista_servidores;

        $lista_coordenadores = array('' => 'Selecionar');
        foreach ($coordenadores as $coordenador) {
            $lista_coordenadores['' . $coordenador->_id . ''] = $coordenador->servidor->nome;
        }

        $data['lista_coordenadores'] = $lista_coordenadores;

        $lista_locais = array('' => 'Selecionar');
        foreach ($locais as $local) {
            $lista_locais['' . $local->_id . ''] = $local->nome;
        }

        $data['lista_locais'] = $lista_locais;
        
        if ($this->input->post('btn_salvar')) {
            $this->form_validation->set_rules('local', 'Local', 'trim|required');
            $this->form_validation->set_rules('servidor', 'Servidor', 'trim|required');
            $this->form_validation->set_rules('coordenador', 'Coordenador', 'trim|required');
            $this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');
            $this->form_validation->set_rules('responsavel', 'Responsavel', 'trim|required');
            $this->form_validation->set_rules('dataSolicitacao', 'Data Solicitacao', 'trim|required');
            $this->form_validation->set_rules('dataInicial', 'Data Inicial', 'trim|required');
            $this->form_validation->set_rules('dataFinal', 'Data Final', 'trim|required');
            $this->form_validation->set_rules('dataAutorizacao', 'Data Autorizacao', 'trim|required');

            $servidor_selecionado = $this->servidor_model->pesquisar_id(intval($this->input->post('servidor')));
            $coordenador_selecionado = $this->coordenador_model->pesquisar_id(intval($this->input->post('coordenador')));
            $local_selecionado = $this->local_model->pesquisar_id(intval($this->input->post('local')));
            
            
            $dataSolicitacao = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataSolicitacao')));
            $dataInicial = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataInicial')));
            $dataFinal = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataFinal')));
            $dataAutorizacao = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataAutorizacao')));
           
            if ($this->form_validation->run() !== FALSE) {
                $result = $this->reserva_model->editar(
                    intval($_id),
                    $this->input->post('tipo'),
                    $this->input->post('status'),
                    $this->input->post('responsavel'),
                    $dataSolicitacao,
                    $dataInicial,
                    $dataFinal,
                    $dataAutorizacao,
                    $servidor_selecionado->_id,
                    $coordenador_selecionado->_id,
                    $local_selecionado->_id

                );
                if ($result === TRUE) {
                    redirect('/reserva');
                } else {
                    $data['error'] = 'Erro ao editar!';

                    $this->load->view('reserva/reserva_editar', $data);
                }
            } else {
                $data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('reserva/reserva_editar', $data);
            }
        } else {
            $reserva = $this->reserva_model->pesquisar_id(intval($_id));
            
            $reserva->servidor = $this->servidor_model->pesquisar_id(intval($reserva->servidorId));
            $reserva->coordenador = $this->coordenador_model->pesquisar_id(intval($reserva->coordenadorId));
            $reserva->local = $this->local_model->pesquisar_id(intval($reserva->localId));
            
            $data['reserva'] = $reserva;
            $this->load->view('reserva/reserva_editar', $data);
        }
    }

    function excluir($_id)
    {
        if ($_id) {
            $this->reserva_model->excluir(intval($_id));
        }
        redirect('/reserva');
    }
}
