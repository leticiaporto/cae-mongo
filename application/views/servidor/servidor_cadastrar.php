<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>CAE</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('../../cae/assets/img/icone_ifba.png'); ?>">
    <script src="../../cae/assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../../cae/assets/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="../../cae/assets/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
</head>

<script>
$(document).ready(function(){
     $('.telefone').mask("(99) 99999-9999");
     $('.cpf').mask("999.999.999-99");
});
</script>
<body class="bg-light">
<div class="container">
	<div class="py-5 text-center">
		<h2>Cadastrar Servidor</h2>
	</div>
	<div class="order-md-1">
     		<?php echo form_open("servidor/cadastrar", array('role' => 'form')); ?>
	        <div class="form-row">
				<div class="form-group col-md-4">
					<label for="nome">Nome:</label>
					<?php echo form_input(array('name' => 'nome', 'class' => 'form-control', 'placeholder' => 'Nome do Servidor'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="cpf">CPF:</label>
					<?php echo form_input(array('name' => 'cpf', 'class' => 'form-control cpf', 'placeholder' => 'CPF'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="siape">Siape:</label>
					<?php echo form_input(array('name' => 'siape', 'class' => 'form-control', 'placeholder' => 'SIAPE'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="login">Login:</label>
					<?php echo form_input(array('name' => 'login', 'class' => 'form-control', 'placeholder' => 'Login'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="senha">Senha:</label>
					<?php echo form_input(array('name' => 'senha', 'class' => 'form-control', 'placeholder' => 'Senha', 'type'=>'password'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="telefone">Telefone:</label>
					<?php echo form_input(array('name' => 'telefone', 'class' => 'form-control telefone', 'placeholder' => 'Telefone',  'data-mask' => "(00) 00000-0000"), '', 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="tipo">Tipo:</label>
					<?php 
			            $lista_tipos = array(
		                    '' => 'Selecionar',
		                    'DOCENTE' => 'DOCENTE',
		                    'TECNICO ADMINISTRATIVO' => 'TECNICO ADMINISTRATIVO',
			            );
						echo form_dropdown(array('name' => 'tipo', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_tipos, '', 'required'); 
					?>

				</div>
				<div class="col-md-12">
					<div class="text-right">
						<?php echo form_submit(array('name' => 'btn_cadastrar', 'type' => 'submit', 'class' => 'btn btn-success mr-2 hidden', 'id' => 'btn_cadastrar'), 'Cadastrar'); ?>
						<a href="<?php echo base_url('servidor'); ?>" class="btn btn-outline-secondary">Voltar</a>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
      	</form>   
	</div>
</div>

</body>

</html>