<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>CAE</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('../../cae/assets/img/icone_ifba.png'); ?>">
    <script src="../../assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../../assets/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>
<body class="bg-light">
<div class="container">
  <div class="py-5 text-center">
    <h2>Editar Coordenador</h2>
  </div>
	<div class="order-md-1">
     		<?php echo form_open("coordenador/editar/$coordenador->_id", array('role' => 'form')); ?>
			 <div class="form-row">
				<div class="form-group col-md-6">
					<label for="bloco">Servidor:</label>
					<?php
						echo form_dropdown(array('name' => 'servidor', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_servidores, $coordenador->servidor->servidorId, 'required'); 
					?>

				</div>
				<div class="form-group col-md-6">
					<label for="nome">Cargo:</label>
					<?php echo form_input(array('name' => 'cargo', 'class' => 'form-control', 'placeholder' => 'Nome do coordenador'), $coordenador->cargo, 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="cpf">Data Inicial:</label>
					<?php echo form_input(array('name' => 'dataInicial', 'type'=>'datetime-local', 'class' => 'form-control', 'placeholder' => 'CPF'), $coordenador->dataInicial->toDateTime()->format('Y-m-d\TH:i:s'), 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="siape">Data Final:</label>
					<?php echo form_input(array('name' => 'dataFinal', 'type'=>'datetime-local', 'class' => 'form-control', 'placeholder' => 'SIAPE'), $coordenador->dataFinal->toDateTime()->format('Y-m-d\TH:i:s'), 'required'); ?>
				</div>
				<div class="col-md-12">
					<div class="text-right">
						<?php echo form_submit(array('name' => 'btn_salvar', 'type' => 'submit', 'class' => 'btn btn-success mr-2 hidden', 'id' => 'btn_salvar'), 'Salvar'); ?>
						<a href="<?php echo base_url('coordenador'); ?>" class="btn btn-outline-secondary">Voltar</a>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
      	</form>   
	</div>
</div>

</body>

</html>