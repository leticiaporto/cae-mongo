<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coordenador extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
        $this->load->model('coordenador_model');
        $this->load->model('servidor_model');
		$this->load->model('bloco_model');
	}
	 
	function index() {
        $data['coordenadores'] = $this->coordenador_model->buscar();
		$this->load->view('coordenador/coordenador_listar', $data);
	}
	
	public function cadastrar() {
        
        $last_id = $this->coordenador_model->lastId();
        $id = 0;
        foreach($last_id as $b){
            $id  = $b->_id;
		}
		$novoId = $id+1;

        $servidores = $this->servidor_model->buscar();
        
        $lista_servidores = array('' => 'Selecionar');		
		foreach($servidores as $servidor){
			$lista_servidores[''.$servidor->_id.''] = $servidor->nome;
        }		
        
        $data['lista_servidores'] = $lista_servidores;
        
        if ($this->input->post('btn_cadastrar')) {
            $this->form_validation->set_rules('cargo', 'Cargo', 'trim|required');
            $this->form_validation->set_rules('dataInicial', 'DataInicial', 'trim|required');
            $this->form_validation->set_rules('dataFinal', 'DataFinal', 'trim|required');
            $this->form_validation->set_rules('servidor', 'Servidor', 'trim|required');

            $servidor_selecionado = $this->servidor_model->pesquisar_id(intval($this->input->post('servidor')));

            $dataInicial = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataInicial')));
            $dataFinal = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataFinal')));

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->coordenador_model->cadastrar(
                    $novoId, 
                    $this->input->post('cargo'), 
                    $dataInicial,
                    $dataFinal,
                    $servidor_selecionado->_id,
                    $servidor_selecionado->nome

                );
				if($result === TRUE) {
					redirect('coordenador');
				} else {
					$data['error'] = 'Erro ao cadastrar!';
					$this->load->view('coordenador/coordenador_cadastrar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('coordenador/coordenador_cadastrar', $data);
            }
        } 
        else {
            $this->load->view('coordenador/coordenador_cadastrar', $data);
        }
    }
	
	function editar($_id) {

        $servidores = $this->servidor_model->buscar();
        
        $lista_servidores = array('' => 'Selecionar');		
		foreach($servidores as $servidor){
			$lista_servidores[''.$servidor->_id.''] = $servidor->nome;
        }		
        
        $data['lista_servidores'] = $lista_servidores;

		if ($this->input->post('btn_salvar')) {
            $this->form_validation->set_rules('cargo', 'Cargo', 'trim|required');
            $this->form_validation->set_rules('dataInicial', 'DataInicial', 'trim|required');
            $this->form_validation->set_rules('dataFinal', 'DataFinal', 'trim|required');

            $servidor_selecionado = $this->servidor_model->pesquisar_id(intval($this->input->post('servidor')));
  
            $dataInicial = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataInicial')));
            $dataFinal = new MongoDB\BSON\UTCDateTime(new DateTime($this->input->post('dataFinal')));

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->coordenador_model->editar(
                	intval($_id), 
                    $this->input->post('cargo'), 
                    $dataInicial,
                    $dataFinal,
                    $servidor_selecionado->_id,
                    $servidor_selecionado->nome
                	
                );
                if($result === TRUE) {
					redirect('/coordenador');
				} else {
                    $data['error'] = 'Erro ao editar!';

					$this->load->view('coordenador/coordenador_editar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('coordenador/coordenador_editar', $data);
            }
        } else {
			$data['coordenador'] = $this->coordenador_model->pesquisar_id(intval($_id));
            $this->load->view('coordenador/coordenador_editar', $data);
        }
	}
	
	function excluir($_id) {
		if ($_id) {
            $this->coordenador_model->excluir(intval($_id));
        }
		redirect('/coordenador');
	}
	
}