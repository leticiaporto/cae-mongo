<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>CAE</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('../../cae/assets/img/icone_ifba.png'); ?>">
    <script src="../../cae/assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../../cae/assets/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="../../cae/assets/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
</head>

<script>
$(document).ready(function(){
     $('.telefone').mask("(99) 99999-9999");
     $('.cpf').mask("999.999.999-99");
});
</script>
<body class="bg-light">
<div class="container">
	<div class="py-5 text-center">
		<h2>Cadastrar Reserva</h2>
	</div>
	<div class="order-md-1">
     		<?php echo form_open("reserva/cadastrar", array('role' => 'form')); ?>
	        <div class="form-row">
				<div class="form-group col-md-4">
					<label for="nome">Nome Responsavel:</label>
					<?php echo form_input(array('name' => 'responsavel', 'class' => 'form-control', 'placeholder' => 'Nome Responsável'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="tipo">Tipo:</label>
					<?php 
			            $lista_tipos = array(
		                    '' => 'Selecionar',
                            'DIARIO' => 'DIÁRIO',
                            'SEMESTRAL' => 'SEMESTRAL',
		                    'MENSAL' => 'MENSAL',
			            );
						echo form_dropdown(array('name' => 'tipo', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_tipos, '', 'required'); 
					?>
				</div>
				<div class="form-group col-md-4">
					<label for="siape">Status:</label>
					<?php 
			            $lista_status = array(
		                    '' => 'Selecionar',
                            'SOLICITADO' => 'SOLICITADO',
                            'CANCELADO' => 'CANCELADO',
                            'RESERVADO' => 'RESERVADO',
			            );
						echo form_dropdown(array('name' => 'status', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_status, '', 'required'); 
					?>
				</div>
				<div class="form-group col-md-4">
					<label for="login">Data Autorização:</label>
					<?php echo form_input(array('name' => 'dataAutorizacao', 'type'=>'datetime-local' ,'class' => 'form-control', 'placeholder' => 'Data Autorizacao'), '', 'required'); ?>
                </div>
                <div class="form-group col-md-4">
					<label for="login">Data Solicitação:</label>
					<?php echo form_input(array('name' => 'dataSolicitacao', 'type'=>'datetime-local' ,'class' => 'form-control', 'placeholder' => 'Data Solicitacao'), '', 'required'); ?>
                </div>
                <div class="form-group col-md-4">
					<label for="login">Data Inicial:</label>
					<?php echo form_input(array('name' => 'dataInicial', 'type'=>'datetime-local' ,'class' => 'form-control', 'placeholder' => 'Data Inicial'), '', 'required'); ?>
                </div>
                <div class="form-group col-md-4">
					<label for="login">Data Final:</label>
					<?php echo form_input(array('name' => 'dataFinal', 'type'=>'datetime-local' ,'class' => 'form-control', 'placeholder' => 'Data Final'), '', 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="senha">Local:</label>
                    <?php
						echo form_dropdown(array('name' => 'local', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_locais, '', 'required'); 
					?>
				</div>
				<div class="form-group col-md-4">
					<label for="telefone">Servidor:</label>
                    <?php
						echo form_dropdown(array('name' => 'servidor', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_servidores, '', 'required'); 
					?>
                </div>
                <div class="form-group col-md-4">
					<label for="telefone">Coordenador:</label>
                    <?php
						echo form_dropdown(array('name' => 'coordenador', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_coordenadores, '', 'required'); 
					?>
				</div>
				<div class="col-md-12">
					<div class="text-right">
						<?php echo form_submit(array('name' => 'btn_cadastrar', 'type' => 'submit', 'class' => 'btn btn-success mr-2 hidden', 'id' => 'btn_cadastrar'), 'Cadastrar'); ?>
						<a href="<?php echo base_url('reserva'); ?>" class="btn btn-outline-secondary">Voltar</a>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
      	</form>   
	</div>
</div>

</body>

</html>