<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servidor extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
		$this->load->model('servidor_model');
		$this->load->model('bloco_model');
	}
	 
	function index() {
		$data['servidores'] = $this->servidor_model->buscar();
		$this->load->view('servidor/servidor_listar', $data);
	}
	
	public function cadastrar() {
		
		$last_id = $this->servidor_model->lastId();
        $id = 0;
        foreach($last_id as $b){
            $id  = $b->_id;
		}
		$novoId = $id+1;

        if ($this->input->post('btn_cadastrar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('siape', 'Siape', 'trim|required');
            $this->form_validation->set_rules('login', 'Login', 'trim|required');
			$this->form_validation->set_rules('senha', 'Senha', 'trim|required');
			$this->form_validation->set_rules('cpf', 'CPF', 'trim|required');
			$this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');
			$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->servidor_model->cadastrar(
					$novoId,
                	$this->input->post('siape'), 
                	$this->input->post('nome'), 
                	$this->input->post('login'), 
                	password_hash($this->input->post("senha"), PASSWORD_BCRYPT, ["cost" => 12]), 
                	$this->input->post('cpf'), 
					$this->input->post('telefone'), 
					$this->input->post('tipo')
                );
				if($result === TRUE) {
					redirect('servidor');
				} else {
					$data['error'] = 'Erro ao cadastrar!';
					$this->load->view('servidor/servidor_cadastrar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('servidor/servidor_cadastrar', $data);
            }
        } 
        else {
            $this->load->view('servidor/servidor_cadastrar', '');
        }
    }
	
	function editar($_id) {

		if ($this->input->post('btn_salvar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('siape', 'Siape', 'trim|required');
            $this->form_validation->set_rules('login', 'Login', 'trim|required');
			$this->form_validation->set_rules('senha', 'Senha', 'trim');
			$this->form_validation->set_rules('cpf', 'CPF', 'trim|required');
			$this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');
			$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->servidor_model->editar(
                	intval($_id), 
                	$this->input->post('siape'), 
                	$this->input->post('nome'), 
                	$this->input->post('login'), 
                	$this->input->post('senha') ? password_hash($this->input->post("senha"), PASSWORD_BCRYPT, ["cost" => 12]) : '', 
                	$this->input->post('cpf'), 
					$this->input->post('telefone'), 
					$this->input->post('tipo')
                );
                if($result === TRUE) {
					redirect('/servidor');
				} else {
                    $data['error'] = 'Erro ao editar!';

					$this->load->view('servidor/servidor_editar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('servidor/servidor_editar', $data);
            }
        } else {
			$data['servidor'] = $this->servidor_model->pesquisar_id(intval($_id));
            $this->load->view('servidor/servidor_editar', $data);
        }
	}
	
	function excluir($_id) {
		if ($_id) {
            $this->servidor_model->excluir(intval($_id));
        }
		redirect('/servidor');
	}
	
}