<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bloco extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
		$this->load->model('bloco_model');
	}
	 
	function index() {
		$data['blocos'] = $this->bloco_model->buscar();
		$this->load->view('bloco/bloco_listar', $data);
	}
	
	public function cadastrar() {
		// var_dump($this->input->post());
		// die();
		$last_id = $this->bloco_model->lastId();
        $id = 0;
        foreach($last_id as $b){
            $id  = $b->_id;
		}
		$novoId = $id+1;
		
        if ($this->input->post('btn_cadastrar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->bloco_model->cadastrar(
					$novoId,
                	$this->input->post('nome'), 
                	$this->input->post('descricao')
                );
				if($result === TRUE) {
					redirect('bloco');
				} else {
					$data['error'] = 'Erro ao cadastrar!';
					$this->load->view('bloco/bloco_cadastrar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('bloco/bloco_cadastrar', $data);
            }
        } 
        else {
            $this->load->view('bloco/bloco_cadastrar');
        }
    }
	
	function editar($_id) {
		if ($this->input->post('btn_editar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
			
            if ($this->form_validation->run() !== FALSE) {
                $result = $this->bloco_model->editar(
                	intval($_id), 
                	$this->input->post('nome'), 
                	$this->input->post('descricao')
                );
                if($result === TRUE) {
					redirect('/bloco');
				} else {
					$data['error'] = 'Erro ao editar!';
					$this->load->view('bloco/bloco_editar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('bloco/bloco_editar', $data);
            }
        } else {
			$data['bloco'] = $this->bloco_model->pesquisar_id(intval($_id));
            $this->load->view('bloco/bloco_editar', $data);
        }
	}
	
	function excluir($_id) {
		if ($_id) {
            $this->bloco_model->excluir(intval($_id));
        }
		redirect('/bloco');
	}
	
}