<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local_model extends CI_model {
	
	private $database = 'cvtweb';
	private $collection = 'locais';
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('mongodb');
		$this->conn = $this->mongodb->getConexao();
	}
	
	function buscar() {
		try {
			$filter = [];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
	function pesquisar_id($_id) {
		try {
			$filter = ['_id' =>$_id];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);
			
			foreach($result as $local) {
				return $local;
			}
			
			return NULL;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching local: ' . $ex->getMessage(), 500);
		}
	}
	
	function cadastrar(
		$novoId, $nome, $numeroChave, $capacidade, 
		$bloco_nome, $bloco_descricao, $bloco_id, 
		$coordenador_id, $coordenador_nome, $coordenador_cargo, $coordenador_data_inicial, $coordenador_data_final,
		$status) {
		try {
			$local = array(
				'_id' => $novoId,
				'nome' => $nome,
				'numeroChave' => $numeroChave,
				'capacidade' => $capacidade,
				'bloco' => array('id_bloco'=>$bloco_id, 'nome'=>$bloco_nome, 'descricao'=>$bloco_descricao),	
				'coordenador' => array('id_coordenador'=>$coordenador_id, 'nome'=>$bloco_nome, 'cargo'=>$coordenador_cargo, 'dataInicial'=>$coordenador_data_inicial, 'dataFinal'=>$coordenador_data_final),
				'status' => $status
			);
			
			$query = new MongoDB\Driver\BulkWrite();
			$query->insert($local);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while saving users: ' . $ex->getMessage(), 500);
		}
	}
	
	function editar($_id, $nome, $numeroChave, $capacidade, 
	$bloco_nome, $bloco_descricao, $bloco_id, 
	$coordenador_id, $coordenador_nome, $coordenador_cargo, $coordenador_data_inicial, $coordenador_data_final,
	$status) {
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->update(['_id' => $_id], 
			['$set' => 
				array('nome' => $nome, 
				'numeroChave' => $numeroChave, 
				'capacidade' => $capacidade, 
				'bloco' => array('id_bloco'=>$bloco_id, 'nome'=>$bloco_nome, 'descricao'=>$bloco_descricao),
				'coordenador' => array('id_coordenador'=>$coordenador_id, 'nome'=>$coordenador_nome, 'cargo'=>$coordenador_cargo, 'dataInicial'=>$coordenador_data_inicial, 'dataFinal'=>$coordenador_data_final), 
				'status' => $status)
			]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while updating users: ' . $ex->getMessage(), 500);
		}
	}
	
	function excluir($_id) {
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->delete(['_id' => $_id]);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);
			
			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	public function lastId(){
		try {
			$filter = [];
			$options = ['sort' => ['_id' => 1]];
			$query = new MongoDB\Driver\Query($filter, $options);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching users: ' . $ex->getMessage(), 500);
		}
	}
	
}