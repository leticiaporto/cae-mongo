<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>CAE</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('../../cae/assets/img/icone_ifba.png'); ?>">
    <script src="../../assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../../assets/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>
<body class="bg-light">
<div class="container">
  <div class="py-5 text-center">
    <h2>Editar Servidor</h2>
  </div>
	<div class="order-md-1">
     		<?php echo form_open("servidor/editar/$servidor->_id", array('role' => 'form')); ?>
			 <div class="form-row">
				<div class="form-group col-md-4">
					<label for="nome">Nome:</label>
					<?php echo form_input(array('name' => 'nome', 'class' => 'form-control', 'placeholder' => 'Nome do Servidor'), $servidor->nome, 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="cpf">CPF:</label>
					<?php echo form_input(array('name' => 'cpf', 'class' => 'form-control cpf', 'placeholder' => 'CPF'), $servidor->CPF, 'required'); ?>
				</div>
				<div class="form-group col-md-4">
					<label for="siape">Siape:</label>
					<?php echo form_input(array('name' => 'siape', 'class' => 'form-control', 'placeholder' => 'SIAPE'), $servidor->siape, 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="login">Login:</label>
					<?php echo form_input(array('name' => 'login', 'class' => 'form-control', 'placeholder' => 'Login'), $servidor->login, 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="senha">Senha:</label>
					<?php echo form_input(array('name' => 'senha', 'class' => 'form-control', 'placeholder' => '', 'type'=>'password'), '', ''); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="telefone">Telefone:</label>
					<?php echo form_input(array('name' => 'telefone', 'class' => 'form-control telefone', 'placeholder' => 'Telefone',  'data-mask' => "(00) 00000-0000"), $servidor->telefone, 'required'); ?>
				</div>
				<div class="form-group col-md-6">
					<label for="tipo">Tipo:</label>
					<?php 
			            $lista_tipos = array(
		                    '' => 'Selecionar',
		                    'DOCENTE' => 'DOCENTE',
		                    'TECNICO ADMINISTRATIVO' => 'TECNICO ADMINISTRATIVO',
			            );
						echo form_dropdown(array('name' => 'tipo', 'class' => 'form-control', 'data-live-search' => 'true', 'placeholder' => ''), $lista_tipos, $servidor->tipo, 'required'); 
					?>

				</div>
				<div class="col-md-12">
					<div class="text-right">
						<?php echo form_submit(array('name' => 'btn_salvar', 'type' => 'submit', 'class' => 'btn btn-success mr-2 hidden', 'id' => 'btn_salvar'), 'Salvar'); ?>
						<a href="<?php echo base_url('servidor'); ?>" class="btn btn-outline-secondary">Voltar</a>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
      	</form>   
	</div>
</div>

</body>

</html>