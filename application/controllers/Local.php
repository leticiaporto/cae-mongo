<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
		$this->load->model('local_model');
		$this->load->model('bloco_model');
		$this->load->model('coordenador_model');
	}
	 
	function index() {
		$locais = $this->local_model->buscar();
        $lista_locais = array();

        foreach ($locais as $local) {
			$lista_locais['' . $local->_id . ''] = $local;
			
            //$local = $this->coordenador_model->pesquisar_id(intval($local->coordenador->coordenadorId));
        }

		$data['locais'] = $this->local_model->buscar();
		$this->load->view('local/local_listar', $data);
	}
	
	public function cadastrar() {
		
		$blocos = $this->bloco_model->buscar();
		$lista_blocos = array('' => 'Selecionar');		
		foreach($blocos as $bloco){
			$lista_blocos[''.$bloco->_id.''] = $bloco->nome;
		}		
		$data['lista_blocos'] = $lista_blocos;

		$coordenadores = $this->coordenador_model->buscar();
		$lista_coordenadores = array('' => 'Selecionar');		
		foreach($coordenadores as $coordenador){
			$lista_coordenadores[''.$coordenador->_id.''] = $coordenador->servidor->nome;
		}		
		$data['lista_coordenadores'] = $lista_coordenadores;

		$last_id = $this->local_model->lastId();
        $id = 0;
        foreach($last_id as $b){
            $id  = $b->_id;
		}
		$novoId = $id+1;

        if ($this->input->post('btn_cadastrar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('numero_chave', 'Número da Chave', 'trim|required');
            $this->form_validation->set_rules('capacidade', 'Capacidade', 'trim|required');
			$this->form_validation->set_rules('bloco', 'Bloco', 'trim|required');
			$this->form_validation->set_rules('coordenador', 'Coordenador', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			
			$bloco_selecionado = $this->bloco_model->pesquisar_id(intval($this->input->post('bloco')));
			$coordenador_selecionado = $this->coordenador_model->pesquisar_id(intval($this->input->post('coordenador')));

            if ($this->form_validation->run() !== FALSE) {
                $result = $this->local_model->cadastrar(
					$novoId,
                	$this->input->post('nome'), 
                	$this->input->post('numero_chave'), 
                	$this->input->post('capacidade'), 
					$bloco_selecionado->nome,
					$bloco_selecionado->descricao,
					$bloco_selecionado->_id,
					$coordenador_selecionado->_id,
					$coordenador_selecionado->servidor->nome,
					$coordenador_selecionado->cargo,
					$coordenador_selecionado->dataInicial,
					$coordenador_selecionado->dataFinal,
					$this->input->post('status')
                );
				if($result === TRUE) {
					redirect('local');
				} else {
					$data['error'] = 'Erro ao cadastrar!';
					$this->load->view('local/local_cadastrar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('local/local_cadastrar', $data);
            }
        } 
        else {
            $this->load->view('local/local_cadastrar', $data);
        }
    }
	
	function editar($_id) {
		
		$blocos = $this->bloco_model->buscar();
		$lista_blocos = array('' => 'Selecionar');		
		foreach($blocos as $bloco){
			$lista_blocos[''.$bloco->_id.''] = $bloco->nome;
		}		
		$data['lista_blocos'] = $lista_blocos;

		$coordenadores = $this->coordenador_model->buscar();
		$lista_coordenadores = array('' => 'Selecionar');		
		foreach($coordenadores as $coordenador){
			$lista_coordenadores[''.$coordenador->_id.''] = $coordenador->servidor->nome;
		}		
		$data['lista_coordenadores'] = $lista_coordenadores;

		if ($this->input->post('btn_editar')) {
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('numero_chave', 'Número da Chave', 'trim|required');
			$this->form_validation->set_rules('capacidade', 'Capacidade', 'trim|required');
			$this->form_validation->set_rules('bloco', 'Bloco', 'trim|required');
			$this->form_validation->set_rules('coordenador', 'Coordenador', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');

			$bloco_selecionado = $this->bloco_model->pesquisar_id(intval($this->input->post('bloco')));
			$coordenador_selecionado = $this->coordenador_model->pesquisar_id(intval($this->input->post('coordenador')));
			
            if ($this->form_validation->run() !== FALSE) {
                $result = $this->local_model->editar(
                	intval($_id), 
                	$this->input->post('nome'), 
                	$this->input->post('numero_chave'), 
                	$this->input->post('capacidade'), 
					$bloco_selecionado->nome,
					$bloco_selecionado->descricao,
					$bloco_selecionado->_id,
					$coordenador_selecionado->_id,
					$coordenador_selecionado->servidor->nome,
					$coordenador_selecionado->cargo,
					$coordenador_selecionado->dataInicial,
					$coordenador_selecionado->dataFinal,
					$this->input->post('status'),
                );
                if($result === TRUE) {
					redirect('/local');
				} else {
					$data['error'] = 'Erro ao editar!';
					$this->load->view('local/local_editar', $data);
				}
            } else {
				$data['error'] = 'Todos os campos são obrigatórios.';
                $this->load->view('local/local_editar', $data);
            }
        } else {
			$data['local'] = $this->local_model->pesquisar_id(intval($_id));
            $this->load->view('local/local_editar', $data);
        }
	}
	
	function excluir($_id) {
		
		if (intval($_id)) {
            $this->local_model->excluir(intval($_id));
        }
		redirect('/local');
	}
	
}