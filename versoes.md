# Cae

### Versão 0.0.10
- Correções e id autoincrement manual

### Versão 0.0.9
- Mudanças consultas e models

### Versão 0.0.8
- Vinculo de Coordenador com Local

### Versão 0.0.7
- Inclusão de Coordenador e vinculação com servidor

### Versão 0.0.6
- Inclusão de Servidor

### Versão 0.0.5
- Finalização do select bloco no local_cadastrar e local_editar

### Versão 0.0.4
- Inclusão select bloco no local

### Versão 0.0.3
- Alteração do cvt/assets para cvtweb/assets

### Versão 0.0.2
- CRUD de Equipamento

### Versão 0.0.1
- CRUD de Local e de Bloco
