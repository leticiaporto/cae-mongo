
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>CAE  </title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('../../cae/assets/img/icone_ifba.png'); ?>">
    <script src="../../cae/assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="../../cae/assets/js/bootstrap.min.js" ></script>
	  <link data-n-head="true" rel="stylesheet" href="../../cae/assets/css/line-awesome/css/line-awesome.min.css">
	  <link data-n-head="true" rel="stylesheet" href="../../cae/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../cae/assets/css/bootstrap.min.css">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
	  }
	  .active {
          color: #28a745 !important;
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="./../cae/assets/css/dashboard.css" rel="stylesheet">
  </head>
  <body>
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">CAE</a>
        </nav>

        <div class="container-fluid">
        <div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(""); ?>">
                        <span data-feather="home"></span>
                        Início <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("local"); ?>">
                        <span data-feather="anchor"></span>
                        Locais
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("bloco"); ?>">
                        <span data-feather="box"></span>
                        Blocos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("servidor"); ?>">
                        <span data-feather="user"></span>
                        Servidor
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url("coordenador"); ?>">
                        <span data-feather="user-plus"></span>
                        Coordenador
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("reserva"); ?>">
                        <span data-feather="database"></span>
                        Reserva
                        </a>
                    </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div class="container">
					<div class="py-5 text-center">
						<h2>Lista de Coordenadores</h2>
					</div>
					<div>
						<a type="button" class="btn btn-success pull-right" href="<?php echo base_url("coordenador/cadastrar"); ?>">
							<i class="la la-plus"></i>
							Cadastrar
						</a>
					</div>
					<br>
					<div class="mt-5">
						<table class="table">
							<thead class="thead-light">
								<tr>
                  <th scope="col">#</th>
                  <th scope="col">Nome</th>
									<th scope="col">Cargo</th>
									<th scope="col">Data Inicial</th>
									<th scope="col">Data Final</th>
									<th scope="col">Ações</th>
								</tr>
							</thead>
							<?php
								if (isset($coordenadores) && $coordenadores) {
							?>
								<tbody>
								<?php
									$i = 1;
									foreach ($coordenadores as $coordenador) {
								?>
									<tr>
										<td>
											<?php echo $i; ?>
                    </td>
                    <td>
											<?php echo $coordenador->servidor->nome; ?>
										</td>
										<td>
											<?php echo $coordenador->cargo; ?>
										</td>
										<td>
											<?php echo $coordenador->dataInicial->toDateTime()->format('d/m/Y H:i:s'); ?>
										</td>
										<td>
											<?php echo $coordenador->dataFinal->toDateTime()->format('d/m/Y H:i:s'); ?>
										</td>
										<td>
											<a  href="<?php echo base_url("coordenador/editar/$coordenador->_id"); ?>" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-success btn-sm"><i class="la la-pencil-square"></i></a>
											<a  href="<?php echo base_url("coordenador/excluir/$coordenador->_id"); ?>" data-toggle="tooltip" data-placement="top" title="Excluir" class="btn btn-danger btn-sm"><i class="la la-trash"></i></a>
										</td>
									</tr>
								<?php
										$i++;
									}
								?>				    
								</tbody>
								<?php
									}
								?>
						</table>
					</div>
				</div>
            </main>
        </div>
        </div>
        <script src="../../cae/assets/js/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="./../cae/assets/js/dashboard.js"></script>
    </body>
</html>
